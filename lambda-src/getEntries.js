require('dotenv').config();
const contentful = require('contentful')

const client = contentful.createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_SPACE_ID
})

const statusCode = 200
const headers = {
  'Access-Control-Allow-Origin' : '*',
  'Access-Control-Allow-Headers': 'Content-Type'
}

exports.handler = function(event, context, callback) {
  if(event.httpMethod !== 'POST' || !event.body) {
    client.getEntries({
      order: '-sys.createdAt',
      include: 6
    })
      .then(x=> {
        callback(null, {
          statusCode,
          headers,
          body: JSON.stringify(x)
        })
      })
  }
}
