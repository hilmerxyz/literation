const dotenv = require('dotenv')
const cmaContentful = require('contentful-management')
const {getConfigForKeys} = require('./lib/config.js')
const ctfConfig = getConfigForKeys([
  'CTF_BLOG_POST_TYPE_ID',
  'CTF_SPACE_ID',
  'CTF_CDA_ACCESS_TOKEN',
  'CTF_CPA_ACCESS_TOKEN',
  'CTF_CMA_ACCESS_TOKEN',
  'CTF_PERSON_ID'
])
const {createClient} = require('./plugins/contentful')
const cdaClient = createClient(ctfConfig)
const cmaClient = cmaContentful.createClient({
  accessToken: ctfConfig.CTF_CMA_ACCESS_TOKEN
})
const pkg = require('./package')
const resolve = require('path').resolve
dotenv.config()


console.log('---------------------------------')
console.log('process.env.LAMBDA_ENDPOINT', process.env.LAMBDA_ENDPOINT)
console.log('---------------------------------')
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A book resource for designers & creatives' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#FFFFFF' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/main.scss'
  ],

  styleResources: {
    scss: ['~/assets/scss/file/media-queries.scss', '~/assets/scss/file/colors.scss', ]
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/contentful'
  ],
  env: {
    LAMBDA_ENDPOINT: process.env.LAMBDA_ENDPOINT,
    CTF_SPACE_ID: process.env.CTF_SPACE_ID,
    CTF_CDA_ACCESS_TOKEN: process.env.CTF_CDA_ACCESS_TOKEN
  },

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    '@nuxtjs/style-resources',
    // '@nuxtjs/dotenv',
    // ['nuxt-sass-resources-loader', [
    //   resolve(__dirname, 'assets/scss/file/media-queries.scss'),
    //   resolve(__dirname, 'assets/scss/file/colors.scss')
    // ]],
    ['@nuxtjs/google-analytics', {
      id: 'UA-120993551-1'
    }]
  ],

  generate: {
    routes () {
      return Promise.all([
        cdaClient.getEntries({
          'content_type': 'title'
        })
      ])
        .then(([entries]) => {
          return [
            ...entries.items.map(entry => `/${entry.fields.slug}`),
          ]
        })
    }
  },

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    // postcss: {
    //   plugins: {
    //     'postcss-cssnext': {
    //       features: {
    //         customProperties: false
    //       }
    //     }
    //   }
    // },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    }
  }
}
