const dotenv = require('dotenv')
const cmaContentful = require('contentful-management')
const {getConfigForKeys} = require('./lib/config.js')
const ctfConfig = getConfigForKeys([
  'CTF_BLOG_POST_TYPE_ID',
  'CTF_SPACE_ID',
  'CTF_CDA_ACCESS_TOKEN',
  'CTF_CPA_ACCESS_TOKEN',
  'CTF_CMA_ACCESS_TOKEN',
  'CTF_PERSON_ID'
])
const {createClient} = require('./plugins/contentful')
const cdaClient = createClient(ctfConfig)
const cmaClient = cmaContentful.createClient({
  accessToken: ctfConfig.CTF_CMA_ACCESS_TOKEN
})
const pkg = require('./package')
const resolve = require('path').resolve
dotenv.config()


return Promise.all([
  cdaClient.getEntries({
    'content_type': 'title'
  })
])
  .then(([entries]) => {
    // console.log('entries.items: ', entries.items)
    return [
      ...entries.items.map(entry => `/${entry.fields.slug}`),
    ]
  })
  .then(x => {
    console.log('x', x)
  })
